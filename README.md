# krazy_kram
Krazy Kram is a real-time communication mobile application written for the purpose of story telling, narration or crowd managemant.

It is written entirely using Flutter, and is cross Platform i.e., runs on both Android and iOS.

Flutter creates native high performing applications using the Dart programming language.

## Getting Started

To run the project locally:

1. Install Flutter. (https://flutter.dev/docs/get-started/install)
2. Run: 'flutter doctor' in terminal after installation. Make sure there are no issues
3. Connect your device/ Run Emulator
4. In Terminal Run: 'flutter devices', make sure your device/devices show up
5. From "ROOT" Project directory enter in terminal: 'flutter run'

App should start.