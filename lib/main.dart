import 'package:flutter/material.dart';
import 'package:krazy_kram/screens/dashboard/homepage.dart';
import './screens/register_login/login_screen.dart';
import './screens/dashboard/admin_dashboard.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: const MaterialColor(0xFFEA0000, const <int,Color>{
          50:const Color(0xFFEA0000),
          100:const Color(0xFFEA0000),
          200:const Color(0xFFEA0000),
          300:const Color(0xFFEA0000),
          400:const Color(0xFFEA0000),
          500:const Color(0xFFEA0000),
          600:const Color(0xFFEA0000),
          700:const Color(0xFFEA0000),
          800:const Color(0xFFEA0000),
          900:const Color(0xFFEA0000)
        }
        ),
      ),
      home: LoginScreen(),
    );
  }
}

// class MyHomePage extends StatefulWidget {
//   MyHomePage({Key key, this.title}) : super(key: key);
//   final String title;

//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(widget.title),
//       ),
//       body: Center(
//         child: Text('Home Screen'),
//       ),
//     );
//   }
// }
