import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:krazy_kram/screens/dashboard/admin_dashboard.dart';
import 'package:krazy_kram/screens/dashboard/client_dashboard.dart';
import 'package:krazy_kram/screens/dashboard/homepage.dart';



class LoginForm extends StatefulWidget {
  @override
  LoginFormState createState() {
    return LoginFormState();
  }
}

class LoginFormState extends State<LoginForm> {
  final _loginFormKey = GlobalKey<FormState>();
  bool _rememberMe = false;
  TextEditingController nameFieldController = TextEditingController();
  TextEditingController passwordFieldController = TextEditingController();

  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([]);      // Look for Alternatives
  }

  @override
  Widget build(BuildContext context){
    return Form(
      key: _loginFormKey,
      child: Padding(
        padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 10.0, top: 0.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Username/E-Mail:',
                style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.w400
                  ),
              ),
              TextFormField(
                obscureText: false,
                controller: nameFieldController,
                style: TextStyle(
                  fontSize: 18,
                  fontFamily: 'Poppins'
                  ),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(10, 15, 10, 15),
                  hintText: 'Enter Your Username',
                  hintStyle: TextStyle(color: Color.fromRGBO(0, 0, 0, 50), fontWeight: FontWeight.w100)
                ),
                validator: (value) {
                  if(value.isEmpty){
                    return 'Please Enter Some Text';
                  }
                  return null;
                },
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
              ),

              Text(
                'Password:',
                style: TextStyle(
                  fontSize: 22,
                  fontFamily: 'Poppins',
                  ),
              ),
              TextFormField(
                controller: passwordFieldController,
                obscureText: true,
                style: TextStyle(
                  fontSize: 18,
                  fontFamily: 'Poppins'
                  ),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(10, 15, 10, 15),
                  hintText: 'Enter Your Password',
                  hintStyle: TextStyle(color: Color.fromRGBO(0, 0, 0, 50), fontWeight: FontWeight.w100)
                ),
                validator: (value) {
                  if(value.isEmpty){
                    return 'Please Enter a Valid Password';
                  }
                  return null;
                },
              ),
              Padding(
                padding: const EdgeInsets.all(7.0),
              ),
              Row(
                children: <Widget>[
                  Checkbox(
                    value: _rememberMe,
                    onChanged: (bool value) {
                      setState(() {
                        _rememberMe = value;
                      });
                    },
                  ),
                  Text(
                    'Remember Me',
                    style: TextStyle(
                      
                      fontSize: 12,
                      fontWeight: FontWeight.w400
                    ),
                    )
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(30),
                  ),
                ButtonTheme(
                minWidth: MediaQuery.of(context).size.width,
                child: RaisedButton(
                color: Color(0xDDEA0000),
                child: Text(
                  'LOGIN',
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'Poppins'
                  ),
                  ),
                
                onPressed: () {
                  print("Logging in");
                  if (_loginFormKey.currentState.validate()){
                    print(passwordFieldController.text);
                    if(nameFieldController.text == 'admin@krazykram.com'){
                      print("Admin Login");
                      Navigator.push(
                        context, 
                        MaterialPageRoute(builder: (context) => AdminDashboard())
                        );
                    }
                    if(nameFieldController.text == 'client@krazykram.com'){
                      print("Client Login");
                      Navigator.push(
                        context, 
                        MaterialPageRoute(builder: (context) => ClientDashboard())
                        );
                    }
                  }

                }
              ),
              )
                ],
              )
            ],
          ),
        ),
    );
  }
}