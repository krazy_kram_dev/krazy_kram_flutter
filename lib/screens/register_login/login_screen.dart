import 'package:flutter/material.dart';
import './login_form.dart';
import './register_screen.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: ListView(
          shrinkWrap: true,
          
          children: <Widget>[
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height/4,
              child: Stack(
                children: <Widget>[
                  Positioned(
                    top: -100,
                    right: -100,
                    child: Container(
                      width: 200.0,
                      height: 200.0,
                      decoration: new BoxDecoration(
                        color: Color(0xFFE84A4A),
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
                  Positioned(
                    top: -10,
                    right: 110,
                    child: Container(
                      width: 150.0,
                      height: 150.0,
                      decoration: new BoxDecoration(
                        color: Color(0xFF2F2E2E),
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 120,
                    right: 50,
                    child: Container(
                      width: 70.0,
                      height: 70.0,
                      decoration: new BoxDecoration(
                        color: Color(0xFFE84A4A),
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 150,
                    right: 250,
                    child: Container(
                      width: 60.0,
                      height: 60.0,
                      decoration: new BoxDecoration(
                        color: Color(0xFFC83E3E),
                        shape: BoxShape.circle,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(10),
              child: Text(
                  'Login',
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Poppins',
                    fontSize: 44,
                    ),
                ),
              ),
          Padding(padding: EdgeInsets.all(50),),
          LoginForm(),
          Padding(padding: EdgeInsets.all(5),),
          Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                'Forgot Password?',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontSize: 12,
                  ),
                ),
                FlatButton(
                  onPressed: () {
                    print("Go To Sign Up");
                    Navigator.push(
                      context, 
                      MaterialPageRoute(builder: (context) => RegisterScreen())
                    );
                  },
                  child: Text(
                    'SIGN UP',
                    style : TextStyle(color: Color(0xFFF30000))
                  ),
                )
              ],
            )
          ),
          ],
        ),
      ),
    );
  }
}