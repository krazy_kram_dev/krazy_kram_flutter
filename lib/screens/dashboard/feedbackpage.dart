import 'package:flutter/material.dart';

class FeedbackPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
            'Feedback Widget',
            style: TextStyle(
              fontFamily: 'Poppins',
              fontSize: 26.0,
              fontWeight: FontWeight.w600,
              decorationStyle: TextDecorationStyle.dotted
            ),
          ),
    );
  }
}