import 'package:flutter/material.dart';
import 'package:krazy_kram/components/custom_buttons.dart';
import 'package:krazy_kram/screens/dashboard/admin_dashboard.dart';
import 'package:krazy_kram/screens/dashboard/client_dashboard.dart';

import './aboutpage.dart';
import './faqpage.dart';
import './feedbackpage.dart';
import './privatepolicypage.dart';
import './sharepage.dart';

class HomePage extends StatefulWidget {
  final bool isAdmin;
  HomePage({Key key, @required this.isAdmin}) : super (key: key);
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  
  int _selectedDrawerIndex;

  _getDrawerItemWidget(int position) {
    print("Access: ${widget}");
    switch(position){
      case 0: return widget.isAdmin ? AdminDashboard() : ClientDashboard(); 
      case 1: return Text('Go Pro Page');
      case 2: return FAQPage();
      case 3: return SharePage();
      case 4: return FeedbackPage();
      case 5: return PrivatePolicyPage();
      case 6: return AboutPage();
      default: return Text('Error! from Drawer Switch State');
    }
  }

  _onDrawerSelect(int index) {
    print("########## $index");
    setState(() => _selectedDrawerIndex = index);
    Navigator.of(context).pop();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _selectedDrawerIndex = 0;
  }
  
  @override
  Widget build(BuildContext context) {

    return(
      Scaffold(
        drawer: Drawer(
          child: ListView(
            children: <Widget>[
              DrawerHeader(
                child: Text(
                  'Hello, Logged in as ${widget.isAdmin}',
                  style: TextStyle(
                    color: Colors.white70,
                    fontFamily: 'Poppins',
                    fontSize: 26.0,
                    fontWeight: FontWeight.w500
                  ),
                  ),
                decoration: BoxDecoration(color: Colors.blue),
              ),
              ListTile(
                leading: Icon(Icons.home),
                title: Text('Dashboard'),
                
                selected: _selectedDrawerIndex == 0,
                onTap: () => _onDrawerSelect(0),
              ),
              ListTile(
                leading: Icon(Icons.stars),
                title: Text('Go Pro. No Ads or Watermarks.'),
                selected: _selectedDrawerIndex == 1,
                onTap: () => _onDrawerSelect(1),
              ),
              ListTile(
                leading: Icon(Icons.question_answer),
                title: Text('FAQ'),
                selected: _selectedDrawerIndex == 2,
                onTap: () => _onDrawerSelect(2),
              ),
              ListTile(
                leading: Icon(Icons.screen_share),
                title: Text('Share with your Friends'),
                selected: _selectedDrawerIndex == 3,
                onTap: () => _onDrawerSelect(3),
              ),
              ListTile(
                leading: Icon(Icons.star),
                title: Text('Give us feedback'),
                selected: _selectedDrawerIndex == 4,
                onTap: () => _onDrawerSelect(4),
              ),
              ListTile(
                leading: Icon(Icons.lock),
                title: Text('Privacy Policy'),
                selected: _selectedDrawerIndex == 5,
                onTap: () => _onDrawerSelect(5),
              ),
              ListTile(
                leading: Icon(Icons.info),
                title: Text('About'),
                selected: _selectedDrawerIndex == 6,
                onTap: () => _onDrawerSelect(6),
              ),
              ListTile(
                leading: Icon(Icons.exit_to_app),
                title: Text('Log Out'),
                selected: false,
                onTap: () {
                  int count = 0;
                  Navigator.of(context).popUntil((_) => count++ >= 2);
                }
              )
            ],
          ),
        ),
        body: Container(

          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(20),
                ),
              Container(
                height: MediaQuery.of(context).size.height - 50,
                child: _getDrawerItemWidget(_selectedDrawerIndex),
              )
            ],
          ),
        ),
      )
    );
  }
}