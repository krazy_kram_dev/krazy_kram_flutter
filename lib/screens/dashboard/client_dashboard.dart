import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:torch/torch.dart';
import '../../components/draw_canvas.dart';
import '../../components/helpers/communication_helper.dart';
import '../../components/custom_buttons.dart';
import '../../components/bottom_sheet_widget.dart';

class ClientDashboard extends StatefulWidget {
  @override
  ClientDashboardState createState() {
    return ClientDashboardState();
  }
}

class ClientDashboardState extends State<ClientDashboard> with TickerProviderStateMixin {

  bool _eventMemoSwitch;
  String _eventMemoText;

  TabController _tabController;
  String _scrollerText;
  double _fontSize;
  Color _fontColor;
  bool _textBlink;
  int _textBlinkFreq;
  bool _backgroundBlink;
  int _backgroundBlinkFreq;
  Color _backgroundColor;
  bool _flashOn;

  AnimationController _scrollerTxtAnimationController;
  AnimationController _backgroundAnimationController;

  @override                     //TODO: Important Dispose unused state variables also do in admin
  void initState() {
    super.initState();
    _eventMemoSwitch = false;
    _eventMemoText = '...ç';
    _tabController = new TabController( length: 2, vsync: this);
    _scrollerText = 'Hi';
    _fontSize = 20;
    _fontColor = Colors.black;
    _textBlink = false;
    _textBlinkFreq = 500;
    _backgroundColor = Color(0xFFC5CAE9);
    _backgroundBlink = false;
    _backgroundBlinkFreq = 500;

    _scrollerTxtAnimationController = new AnimationController(vsync: this, duration: Duration(milliseconds: _textBlinkFreq));
    _scrollerTxtAnimationController.animateTo(_scrollerTxtAnimationController.upperBound);

    _backgroundAnimationController = new AnimationController(vsync: this, duration: Duration(milliseconds: _backgroundBlinkFreq));
    _backgroundAnimationController.animateTo(_backgroundAnimationController.upperBound);

    app.addListener(_onAppDataReceived);
  }

  @override
  void dispose(){
    app.removeListener(_onAppDataReceived);
    _tabController.dispose();
    super.dispose();
  }

  void updateText(String text) {
    setState(() {
      _scrollerText = text;
    });
  }

  void setFontSize(double fontSize) {
    setState(() {
      _fontSize = fontSize;
    });
  }

  void toggleTextBlink(bool blink){
    setState(() {
      _textBlink = blink;
    });
    if(!blink){
      _scrollerTxtAnimationController.reset();
      _scrollerTxtAnimationController.animateTo(_scrollerTxtAnimationController.upperBound, duration: Duration(microseconds: 0));
    }
    else{
      if(_textBlinkFreq == 0){
        _scrollerTxtAnimationController.reset();
        _scrollerTxtAnimationController.animateTo(_scrollerTxtAnimationController.upperBound, duration: Duration(microseconds: 0));
        return;
      }   
      _scrollerTxtAnimationController.repeat();
    }
  }

  void setTextBlinkFrequency(int frequency){
    setState(() {
      _textBlinkFreq = frequency;
    });
    if(_textBlink){
      if(frequency > 0){
      double dur = (1/frequency)*10000;
      setState(() {
        _scrollerTxtAnimationController.duration = Duration(milliseconds: dur.toInt());
        _scrollerTxtAnimationController.repeat();
      });

    }
    if(frequency == 0){
      _scrollerTxtAnimationController.reset();
      _scrollerTxtAnimationController.animateTo(_scrollerTxtAnimationController.upperBound, duration: Duration(microseconds: 0));
    }
    }
  }

  void toggleBackgroundBlink(bool blink){
    setState(() {
      _backgroundBlink = blink;
    });
    if(!blink){
      _backgroundAnimationController.reset();
      _backgroundAnimationController.animateTo(_backgroundAnimationController.upperBound, duration: Duration(milliseconds: 0));
    }
    else{
      _backgroundAnimationController.repeat();
    }
  }

  void setBackgroundBlinkFrequency(int frequency){
    setState(() {
      _backgroundBlinkFreq = frequency;
    });
    if(_backgroundBlink){
      if(frequency > 0){
      double dur = (1/frequency)*10000;
      setState(() {
        _backgroundAnimationController.duration = Duration(milliseconds: dur.toInt());
        _backgroundAnimationController.repeat();
      });

    }
    if(frequency == 0){
      _backgroundAnimationController.reset();
      _backgroundAnimationController.animateTo(_backgroundAnimationController.upperBound, duration: Duration(microseconds: 0));
    }
    }
  }

  void setTab(){
    print(_tabController.index);
  }

  void toggleFlash(bool state){
    if(state){
      Torch.turnOn();
    }
    else{
      Torch.turnOff();
    }
    setState(() {
      _flashOn = state;
    });
  }

  _onAppDataReceived(message) {
    print("Updated");
    print(message);
    switch (message["action"]) {

        case 'memo':
          print('Memo Data');
          var recdData = json.decode(message["data"]);
          print("Set memo to ${recdData["on"]}");
          if(recdData["on"]){
            print(recdData["text"]);
            setState(() {
              _eventMemoSwitch = true;
              _eventMemoText = recdData["text"];
            });
          }
          else{
            setState(() {
              _eventMemoSwitch = false;
            });
          }
          
          break;
        
        case 'scr_data':
          print("Client Access");
          var recvdScrollData = json.decode(message["data"]);
          print("This");
          toggleTextBlink(recvdScrollData["tblink"]);
          setTextBlinkFrequency(recvdScrollData["tblinkfreq"]);
          toggleBackgroundBlink(recvdScrollData["bckblink"]);
          setBackgroundBlinkFrequency(recvdScrollData["bckblinkfreq"]);
          toggleFlash(recvdScrollData["flash"]);
          setState(() {
            _scrollerText = recvdScrollData["text"];
            _fontSize = recvdScrollData["size"];
            _fontColor = Color(recvdScrollData["color"]);
            _backgroundColor = Color(recvdScrollData["bckcolor"]);
          });
          print("Set State Done");
          break;

          case 'tab':
            print('Tab Changed by Admin');
            print(message["data"]);
            final recvdTapIndex = json.decode(message["data"]);
            print(recvdTapIndex.runtimeType);
            print(recvdTapIndex["data"]);
            setState(() {
              _tabController.index = recvdTapIndex["data"];
            });
            break;
          
      }
  }
  

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      drawer: Drawer(
        child: Center(child: Text('Logged in as Client', style: TextStyle(fontSize: 22),)),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 25.0),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, bottom: 15),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[            
              IconButton(
                  icon: Icon(CustomIcons.menu),
                  tooltip: 'Open Drawer',
                  onPressed: () {
                    print(Scaffold.of(context));
                    Scaffold.of(context).openDrawer();
                  }
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15, right: 15, top: 8, bottom: 8),
            child: _eventMemoSwitch? Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>
              [
                Text(
                      'Event Memo: ',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Poppins'
                      ),
                    ),
                Container(
                  child: Text(
                    _eventMemoText,
                    style: TextStyle(
                        fontSize: 16,
                        fontFamily: 'Poppins'
                      ),
                  ),
                ),
              ]
            ):SizedBox(),
          ),
          TabBar(
            controller: _tabController,
            indicatorColor: Colors.grey,
            labelColor: Colors.greenAccent,
            unselectedLabelColor: Colors.black54,
            isScrollable: false,
            tabs: <Widget>[
              Tab(
                icon: Icon(Icons.keyboard),
                child: Text('Type'),
              ),
              Tab(
                icon: Icon(Icons.book),
                child: Text('Handwriting'),
              )
            ],
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0),
          ),
          Expanded(
            child: TabBarView(
              physics: NeverScrollableScrollPhysics(),
              controller: _tabController,
              children: <Widget>[
                FadeTransition(
                  opacity: _backgroundAnimationController,
                  child: Container(
                    padding: EdgeInsets.only(top: 30),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      color: _backgroundColor,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        FadeTransition(
                          opacity: _scrollerTxtAnimationController,
                          child: Text(
                            _scrollerText,
                            style: TextStyle(
                              fontSize: _fontSize,
                              fontWeight: FontWeight.w700,
                              color: _fontColor
                            ),
                          ),
                        )
                      ]
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8.0),
                    color: Colors.orangeAccent,
                  ),
                  child: ClientCanvasWidget(),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}