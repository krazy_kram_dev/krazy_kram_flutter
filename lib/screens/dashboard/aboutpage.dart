import 'package:flutter/material.dart';

class AboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child : Text(
            'About Page',
              style: TextStyle(
              fontFamily: 'Poppins',
              fontSize: 26.0,
              fontWeight: FontWeight.w600,
              decorationStyle: TextDecorationStyle.dotted
            ),
          )
    );
  }
}