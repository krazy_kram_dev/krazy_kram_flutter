import 'package:flutter/material.dart';

class SharePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child : Text(
            'Share Widget',
            style: TextStyle(
              fontFamily: 'Poppins',
              fontSize: 26.0,
              fontWeight: FontWeight.w600,
              decorationStyle: TextDecorationStyle.dotted
            ),
          )
    );
  }
}