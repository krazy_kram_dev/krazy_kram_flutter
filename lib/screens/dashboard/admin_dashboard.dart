import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:torch/torch.dart';
import 'package:animator/animator.dart';

import '../../components/draw_canvas.dart';
import '../../components/helpers/communication_helper.dart';
import '../../components/custom_buttons.dart';
import '../../components/bottom_sheet_widget.dart';



class AdminDashboard extends StatefulWidget {
  @override
  AdminDashboardState createState() {
    return AdminDashboardState();
  }
}

class AdminDashboardState extends State<AdminDashboard> with TickerProviderStateMixin {

  bool _eventMemoSwitch;
  String _eventMemoText;

  TabController _tabController;

  String _scrollerText;
  bool _scrollTextPlay;
  String _scrollDirection;
  double _fontSize;
  Color _fontColor;
  bool _textBlink;
  int _textBlinkFreq;
  bool _backgroundBlink;
  int _backgroundBlinkFreq;
  Color _backgroundColor;
  bool _flashOn;
  Widget _scrollerContainer;

  TextEditingController _eventMemoController;

  AnimationController _scrollerTxtAnimationController;
  AnimationController _backgroundAnimationController;

  @override
  void initState() {
    super.initState();
    _eventMemoSwitch = false;
    _eventMemoText = '';
    _tabController = new TabController( length: 2, vsync: this);
    _scrollerText = 'Hi';
    _scrollTextPlay = false;
    _scrollDirection = 'right';
    _fontSize = 20;
    _fontColor = Colors.black;
    _textBlink = false;
    _textBlinkFreq = 500;
    _backgroundColor = Color(0xFFC5CAE9);
    _backgroundBlink = false;
    _backgroundBlinkFreq = 500;
    _flashOn = false;
    _eventMemoController = new TextEditingController();

    _scrollerTxtAnimationController = new AnimationController(vsync: this, duration: Duration(milliseconds: _textBlinkFreq));
    _scrollerTxtAnimationController.animateTo(_scrollerTxtAnimationController.upperBound);

    _backgroundAnimationController = new AnimationController(vsync: this, duration: Duration(milliseconds: _backgroundBlinkFreq));
    _backgroundAnimationController.animateTo(_backgroundAnimationController.upperBound);

    app.addListener(_onAppDataReceived);
  }

  @override
  void dispose(){
    app.removeListener(_onAppDataReceived);
    _tabController.dispose();
    super.dispose();
  }

  Widget animationWidget(){
    print('Called: ${this._scrollDirection}');
    if(this._scrollTextPlay && this._scrollDirection=='left'){
          return Animator(
          tween:  Tween<Offset>(begin: Offset(10, 0), end: Offset(-10, 0)),
          repeats: 0,
          duration: Duration(seconds: 2),
          builder: (animateCtx) => FractionalTranslation(
            translation: animateCtx.value,
            child: FadeTransition(
              opacity: this._scrollerTxtAnimationController,
                child: Text(
                this._scrollerText,
                style: TextStyle(
                  fontSize: this._fontSize,
                  fontWeight: FontWeight.w700,
                  color: this._fontColor,
                ),
              ),
          ),
          )
        );
      }
    else if(this._scrollTextPlay && this._scrollDirection == 'right'){
      return Animator(
          tween:  Tween<Offset>(begin: Offset(-10, 0), end: Offset(10, 0)),
          repeats: 0,
          duration: Duration(seconds: 2),
          builder: (animateCtx) => FractionalTranslation(
            translation: animateCtx.value,
            child: FadeTransition(
              opacity: this._scrollerTxtAnimationController,
                child: Text(
                this._scrollerText,
                style: TextStyle(
                  fontSize: this._fontSize,
                  fontWeight: FontWeight.w700,
                  color: this._fontColor,
                ),
              ),
          ),
          )
        );
      }
      else if(this._scrollTextPlay && this._scrollDirection == 'up'){
      return Animator(
          tween:  Tween<Offset>(begin: Offset(0, 10), end: Offset(0, -10)),
          repeats: 0,
          duration: Duration(seconds: 2),
          builder: (animateCtx) => FractionalTranslation(
            translation: animateCtx.value,
            child: FadeTransition(
              opacity: this._scrollerTxtAnimationController,
                child: Text(
                this._scrollerText,
                style: TextStyle(
                  fontSize: this._fontSize,
                  fontWeight: FontWeight.w700,
                  color: this._fontColor,
                ),
              ),
          ),
          )
        );
      }
      else if(this._scrollTextPlay && this._scrollDirection == 'down'){
      return Animator(
          tween:  Tween<Offset>(begin: Offset(0, -10), end: Offset(0, 10)),
          repeats: 0,
          duration: Duration(seconds: 2),
          builder: (animateCtx) => FractionalTranslation(
            translation: animateCtx.value,
            child: FadeTransition(
              opacity: this._scrollerTxtAnimationController,
                child: Text(
                this._scrollerText,
                style: TextStyle(
                  fontSize: this._fontSize,
                  fontWeight: FontWeight.w700,
                  color: this._fontColor,
                ),
              ),
          ),
          )
        );
      }
    else if(!this._scrollTextPlay){
      return FadeTransition(
              opacity: this._scrollerTxtAnimationController,
                child: Text(
                this._scrollerText,
                style: TextStyle(
                  fontSize: this._fontSize,
                  fontWeight: FontWeight.w700,
                  color: this._fontColor,
                ),
              ),
          );
    }
    else{
      return Container(
        child: Text('Loading State'),
      );
    }
  }

  void setScrollDirection(String direction){
    setState(() {
      _scrollDirection = direction;
    });
  }

  void updateText(String text) {
    setState(() {
      _scrollerText = text;
    });
  }

  String getText(){
    return _scrollerText;
  }
  void increaseFontSize() {
    setState(() {
      _fontSize = _fontSize + 1;
    });
  }
  void decreaseFontSize() {
    setState(() {
      _fontSize = _fontSize - 1;
    });
  }

  double getFontSize() {
    return _fontSize;
  }

  Color getFontColor() {
    return _fontColor;
  }

  void setFontColor(Color color) {
    print("Set Font Color");
    setState(() {
      _fontColor = color;
    });
  }

  void toggleTextBlink(bool blink){
    setState(() {
      _textBlink = blink;
    });
    if(!blink){
      _scrollerTxtAnimationController.reset();
      _scrollerTxtAnimationController.animateTo(_scrollerTxtAnimationController.upperBound, duration: Duration(microseconds: 0));
    }
    else{
      if(_textBlinkFreq == 0){
        _scrollerTxtAnimationController.reset();
        _scrollerTxtAnimationController.animateTo(_scrollerTxtAnimationController.upperBound, duration: Duration(microseconds: 0));
        return;
      }   
      _scrollerTxtAnimationController.repeat();
    }
  }

  void setTextBlinkFrequency(int frequency){
    setState(() {
      _textBlinkFreq = frequency;
    });
    if(_textBlink){
      if(frequency > 0){
      double dur = (1/frequency)*10000;
      setState(() {
        _scrollerTxtAnimationController.duration = Duration(milliseconds: dur.toInt());
        _scrollerTxtAnimationController.repeat();
      });

    }
    if(frequency == 0){
      _scrollerTxtAnimationController.reset();
      _scrollerTxtAnimationController.animateTo(_scrollerTxtAnimationController.upperBound, duration: Duration(microseconds: 0));
    }
    }
  }

  void setBackgroundColor(Color color) {
    print("Changing BAckgorung Color Now");
    setState(() {
      _backgroundColor = color;
    });
  }

  void toggleBackgroundBlink(bool blink){
    setState(() {
      _backgroundBlink = blink;
    });
    if(!blink){
      _backgroundAnimationController.reset();
      _backgroundAnimationController.animateTo(_backgroundAnimationController.upperBound, duration: Duration(milliseconds: 0));
    }
    else{
      _backgroundAnimationController.repeat();
    }
  }

  void setBackgroundBlinkFrequency(int frequency){
    setState(() {
      _backgroundBlinkFreq = frequency;
    });
    if(_backgroundBlink){
      if(frequency > 0){
      double dur = (1/frequency)*10000;
      setState(() {
        _backgroundAnimationController.duration = Duration(milliseconds: dur.toInt());
        _backgroundAnimationController.repeat();
      });

    }
    if(frequency == 0){
      _backgroundAnimationController.reset();
      _backgroundAnimationController.animateTo(_backgroundAnimationController.upperBound, duration: Duration(microseconds: 0));
    }
    }
  }

  void toggleFlash(bool state){
    if(state){
      Torch.turnOn();
    }
    else{
      Torch.turnOff();
    }
    setState(() {
      _flashOn = state;
    });
  }

  void toggleScrollPlay(){
    print('PLAY: $_scrollTextPlay');
    setState(() {
      _scrollTextPlay = !_scrollTextPlay;
    });
  }

  void sendScrollerDataToServer(){
    print(_scrollerText);
    print(_fontSize);
    Map data = {
      'text': _scrollerText,
      'size': _fontSize,
      'color': _fontColor.value,
      'tblink': _textBlink,
      'tblinkfreq': _textBlinkFreq,
      'bckcolor': _backgroundColor.value,
      'bckblink': _backgroundBlink,
      'bckblinkfreq': _backgroundBlinkFreq,
      'flash': _flashOn
    };
    app.send('scr_data', json.encode(data));
  }


  _onAppDataReceived(message) {
    print(message.runtimeType);
    switch (message["action"]) {
        
        case 'scroller_data': {
          print(message["data"]);
          var recvdScrollData = json.decode(message["data"]);
          print(recvdScrollData.runtimeType);
          print(recvdScrollData["text"]);
          print(recvdScrollData["size"]);
          
          // updateText();
        }
      }
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      drawer: Drawer(
        child: Center(child: Text('Logged in as Admin', style: TextStyle(fontSize: 22),)),
      ),
      body: Stack(
        children: <Widget>[
          Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 25.0),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10, bottom: 15),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[            
                  IconButton(
                      icon: Icon(CustomIcons.menu),
                      tooltip: 'Open Drawer',
                      onPressed: () {
                        print(Scaffold.of(context));
                        Scaffold.of(context).openDrawer();
                      }
                    ),
                    IconButton(
                      icon: Icon(Icons.settings),
                      tooltip: 'Open Settings',
                      onPressed: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context){
                            return AlertDialog(
                              titlePadding: const EdgeInsets.all( 10.0),
                              contentPadding: const EdgeInsets.all(10.0),
                              title: Text(
                                'General Settings',
                                style: TextStyle(
                                  fontSize: 22,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: 'Poppins'
                                ),
                              ),
                              content: Container(
                                width: MediaQuery.of(context).size.width,
                                child: SingleChildScrollView(
                                  child: Column(
                                    
                                    children: <Widget>[
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            'Event Name: ',
                                            style: TextStyle(
                                              fontSize: 16,
                                              fontFamily: 'Poppins'
                                            ),
                                            ),
                                          TextField(
                                            decoration: InputDecoration(
                                              hintText: 'Enter Event Name'
                                            ),
                                          )
                                        ],
                                        ),
                                        Padding(
                                          padding: EdgeInsets.symmetric(vertical: 15),
                                        ),
                                        Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            'Screen Off Message (Idle Mode): ',
                                            style: TextStyle(
                                              fontSize: 16,
                                              fontFamily: 'Poppins'
                                            ),
                                          ),
                                          TextField(
                                            decoration: InputDecoration(
                                              hintText: 'Enter Screen Off Message'
                                            ),
                                          )
                                        ],
                                        ),
                                        Padding(
                                          padding: EdgeInsets.symmetric(vertical: 15),
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: <Widget>[
                                            FlatButton(
                                              child: Text('SAVE'),
                                              onPressed: (){
                                                Navigator.of(context).pop();
                                              },
                                            ),
                                            FlatButton(
                                              child: Text('CANCEL'),
                                              onPressed: (){
                                                Navigator.of(context).pop();
                                              },
                                            )
                                          ],
                                        )
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }
                        );
                      }
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15, top: 4, bottom: 4),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Event Memo: ',
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Poppins'
                      ),
                    ),
                    Switch(
                      value: _eventMemoSwitch,
                      onChanged: (bool val){
                        print("Memo switch at $val");
                        app.send('memo', json.encode({'on': val, "text" : _eventMemoText}));
                        setState(() {
                          _eventMemoSwitch = val;
                        });
                        if(val){

                        }
                      },
                    )
                  ],
                  ),
              ),
              _eventMemoSwitch?
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15, top: 3, bottom: 3),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: MediaQuery.of(context).size.width-130,
                      child: TextField(
                        controller: _eventMemoController,
                        enabled: _eventMemoSwitch,
                        onChanged: (text) {
                          setState(() {
                            _eventMemoText = text;
                          });
                          app.send('memo', json.encode({'on':true, 'text':text}));
                        },
                        decoration: InputDecoration(
                          hintText: 'Type Event Memo Here'
                        ),
                      ),
                    ),
                    RawMaterialButton(
                      onPressed: () {
                        _eventMemoController.clear();
                        app.send('memo', json.encode({'on':true, 'text': ''}));
                      },
                      child: Icon(Icons.cancel),
                    )
                  ],
                ),
              ): SizedBox(),
              TabBar(
                controller: _tabController,
                indicatorColor: Colors.grey,
                labelColor: Colors.greenAccent,
                unselectedLabelColor: Colors.black54,
                isScrollable: false,
                onTap: (int val){
                  print(val);
                  app.send('tab', json.encode({'data':val}));
                },
                tabs: <Widget>[
                  Tab(
                    icon: Icon(Icons.keyboard),
                    child: Text('Type'),
                  ),
                  Tab(
                    icon: Icon(Icons.book),
                    child: Text('Handwriting'),
                  )
                ],
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0),
              ),
              Expanded(
                child: TabBarView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _tabController,
                  children: <Widget>[
                    FadeTransition(
                      opacity: _backgroundAnimationController,
                        child: Container(
                        padding: EdgeInsets.only(top: 30),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          color: _backgroundColor,
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            animationWidget()
                          ]
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        color: Colors.orangeAccent,
                      ),
                      child: CanvasWidget(),
                    ),
                  ],
                ),
              )
            ],
          ),
          BottomSheetWidget(this.updateText, this.toggleScrollPlay, this.setScrollDirection, this.increaseFontSize, this.decreaseFontSize, this.setFontColor, this.toggleTextBlink, this.setTextBlinkFrequency, this.setBackgroundColor, this.toggleBackgroundBlink, this.setBackgroundBlinkFrequency, this.toggleFlash, this.sendScrollerDataToServer)
        ],
      ),
    );
  }
}