  import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

import '../components/helpers/communication_helper.dart';

class CanvasWidget extends StatefulWidget {
  @override
  _CanvasWidgetState createState() => _CanvasWidgetState();
}

class _CanvasWidgetState extends State<CanvasWidget> {
  Color selectedColor = Colors.black;
  Color pickerColor = Colors.black;
  double strokeWidth = 3.0;
  List<DrawingPoints> points = List();
  bool showBottomList = false;
  double opacity = 1.0;
  StrokeCap strokeCap = (Platform.isAndroid) ? StrokeCap.butt : StrokeCap.round;
  SelectedMode selectedMode = SelectedMode.StrokeWidth;
  List<Color> colors = [
    Colors.red,
    Colors.green,
    Colors.blue,
    Colors.amber,
    Colors.black
  ];
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
          decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50.0),
          color: Colors.greenAccent),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                        icon: Icon(Icons.album),
                        onPressed: () {
                          setState(() {
                            if (selectedMode == SelectedMode.StrokeWidth)
                              showBottomList = !showBottomList;
                            selectedMode = SelectedMode.StrokeWidth;
                          });
                        }),
                    IconButton(
                        icon: Icon(Icons.opacity),
                        onPressed: () {
                          setState(() {
                            if (selectedMode == SelectedMode.Opacity)
                              showBottomList = !showBottomList;
                            selectedMode = SelectedMode.Opacity;
                          });
                        }),
                    IconButton(
                        icon: Icon(Icons.color_lens),
                        onPressed: () {
                          setState(() {
                            if (selectedMode == SelectedMode.Color)
                              showBottomList = !showBottomList;
                            selectedMode = SelectedMode.Color;
                          });
                        }),
                    IconButton(
                        icon: Icon(Icons.clear),
                        onPressed: () {
                          setState(() {
                            showBottomList = false;
                            points.clear();
                            app.send('cc', null);     //cc = Clear Canvas
                          });
                        }),
                  ],
                ),
                Visibility(
                  child: (selectedMode == SelectedMode.Color)
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: getColorList(),
                        )
                      : Slider(
                          value: (selectedMode == SelectedMode.StrokeWidth)
                              ? strokeWidth
                              : opacity,
                          max: (selectedMode == SelectedMode.StrokeWidth)
                              ? 50.0
                              : 1.0,
                          min: 0.0,
                          onChanged: (val) {
                            setState(() {
                              if (selectedMode == SelectedMode.StrokeWidth)
                                strokeWidth = val;
                              else
                                opacity = val;
                            });
                            if (selectedMode == SelectedMode.StrokeWidth) {
                              print("Stroke Width: $val"); //Tray updating state after  a buffer
                              app.send('cb-w', json.encode({'wid': val}));
                            }
                            else {
                              print("Stroke Opacity $val");
                              app.send('cb-o', json.encode({'op': val}));
                            }
                          }),
                  visible: showBottomList,
                ),
              ],
            ),
          )
        ),
        Container(
          child: GestureDetector(
            onPanUpdate: (details) {
              setState(() {
                RenderBox renderBox = context.findRenderObject();
                final pointsSet = renderBox.globalToLocal(details.globalPosition);
                points.add(DrawingPoints(
                    points: pointsSet,
                    paint: Paint()
                      ..strokeCap = strokeCap
                      ..isAntiAlias = true
                      ..color = selectedColor.withOpacity(opacity)
                      ..strokeWidth = strokeWidth
                    ));
                app.send('canvas', json.encode({'x': pointsSet.dx.toInt(), 'y' : pointsSet.dy.toInt()}));
                
              });
            },
            onPanStart: (details) {
              setState(() {
                RenderBox renderBox = context.findRenderObject();
                points.add(DrawingPoints(
                    points: renderBox.globalToLocal(details.globalPosition),
                    paint: Paint()
                      ..strokeCap = strokeCap
                      ..isAntiAlias = true
                      ..color = selectedColor.withOpacity(opacity)
                      ..strokeWidth = strokeWidth));
              });
            },
            onPanEnd: (details) {
              setState(() {
                points.add(null);
              });
            },
            child: CustomPaint(
              size: Size(MediaQuery.of(context).size.width, 311),
              painter: DrawingPainter(
                pointsList: points,
              ),
            ),
          ),
        ),
      ],
    );
  }
    getColorList() {
    List<Widget> listWidget = List();
    for (Color color in colors) {
      listWidget.add(colorCircle(color));
    }
    Widget colorPicker = GestureDetector(
      onTap: () {
        showDialog(
          context: context,
          builder: (context)=> AlertDialog(
            title: const Text('Pick a color!'),
            content: SingleChildScrollView(
              child: ColorPicker(
                pickerColor: pickerColor,
                onColorChanged: (color) {
                  pickerColor = color;
                },
                enableLabel: true,
                pickerAreaHeightPercent: 0.8,
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: const Text('Save'),
                onPressed: () {
                  setState(() => selectedColor = pickerColor);
                  Navigator.of(context).pop();
                  print("Color: $pickerColor");
                  app.send('cb-c', json.encode({'clr': pickerColor.value.toString()}));
                },
              ),
            ],
          ),
        );
      },
      child: ClipOval(
        child: Container(
          padding: const EdgeInsets.only(bottom: 16.0),
          height: 36,
          width: 36,
          decoration: BoxDecoration(
              gradient: LinearGradient(
            colors: [Colors.red, Colors.green, Colors.blue],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          )),
        ),
      ),
    );
    listWidget.add(colorPicker);
    return listWidget;
  }

  Widget colorCircle(Color color) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedColor = color;
        });
        app.send('cb-c', json.encode({'clr':color.value.toString()}));
      },
      child: ClipOval(
        child: Container(
          padding: const EdgeInsets.only(bottom: 16.0),
          height: 36,
          width: 36,
          color: color,
        ),
      ),
    );
  }
}

class ClientCanvasWidget extends StatefulWidget {
  @override
  _ClientCanvasWidgetState createState() => _ClientCanvasWidgetState();
}

class _ClientCanvasWidgetState extends State<ClientCanvasWidget> {

  Color selectedColor = Colors.black;
  Color pickerColor = Colors.black;
  double strokeWidth = 3.0;
  List<DrawingPoints> points = List();
  bool showBottomList = false;
  double opacity = 1.0;
  StrokeCap strokeCap = (Platform.isAndroid) ? StrokeCap.butt : StrokeCap.round;
  SelectedMode selectedMode = SelectedMode.StrokeWidth;
  List<Color> colors = [
    Colors.red,
    Colors.green,
    Colors.blue,
    Colors.amber,
    Colors.black
  ];

  @override
  void initState(){
    super.initState();

    app.addListener(_onCanvasDataReceived);
  }

  @override
  void dispose(){
    app.removeListener(_onCanvasDataReceived);
    super.dispose();
  }

  _onCanvasDataReceived(message) {
    print(message['action']);
    switch (message["action"]) {
      case 'canvas':
            print('Received Canvas Data');
            final recvdCanvasData = json.decode(message["data"]);
            // print(recvdCanvasData['x'].runtimeType);
            final recvdPointOffset = new Offset(recvdCanvasData['x'].toDouble(), recvdCanvasData['y'].toDouble());
            print(recvdCanvasData);
            setState(() {
              points.add(DrawingPoints(
              points: recvdPointOffset,
              paint: Paint()
                ..strokeCap = strokeCap
                ..isAntiAlias = true
                ..color = selectedColor.withOpacity(opacity)
                ..strokeWidth = strokeWidth
              ));
            });
            
            break;
          
      case 'cc':
        print("Clear Canvas Signal");
        setState(() {
          points.clear();
        });
        break;
      
      case 'cb-w':          //Color Brush Width
      print("Admin changed width");
      final recvdCanvasBrushData = json.decode(message["data"]);   
      print(recvdCanvasBrushData['wid'].toString());
      setState(() {
        strokeWidth = recvdCanvasBrushData['wid'];
      });            
        break;
      
      case 'cb-c':                //Color Brush Color
        final recvdCanvasBrushData = json.decode(message["data"]);  
        print('recvd Color'); 
        print(recvdCanvasBrushData['clr'].runtimeType);
        setState(() {
          selectedColor = Color(int.parse(recvdCanvasBrushData['clr']));
        });    
        break;

      case 'cb-o':                //Color Brush Opacity
      print("Admin changed Opacity");
        final recvdCanvasBrushData = json.decode(message["data"]);   
        print(recvdCanvasBrushData['op'].toString());  
        setState(() {
          opacity = recvdCanvasBrushData['op'];
        });           
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
          child: CustomPaint(
              size: Size(MediaQuery.of(context).size.width, 311),
              painter: DrawingPainter(
                pointsList: points,
              ),
            ),
        )
      ],
    );
  }
}

class DrawingPainter extends CustomPainter {
  DrawingPainter({this.pointsList});
  List<DrawingPoints> pointsList;
  List<Offset> offsetPoints = List();
  @override
  void paint(Canvas canvas, Size size) {
    for (int i = 0; i < pointsList.length - 1; i++) {
      if (pointsList[i] != null && pointsList[i + 1] != null) {
        canvas.drawLine(pointsList[i].points, pointsList[i + 1].points,
            pointsList[i].paint);
      } else if (pointsList[i] != null && pointsList[i + 1] == null) {
        offsetPoints.clear();
        offsetPoints.add(pointsList[i].points);
        offsetPoints.add(Offset(
            pointsList[i].points.dx + 0.1, pointsList[i].points.dy + 0.1));
        canvas.drawPoints(PointMode.points, offsetPoints, pointsList[i].paint);
      }
    }
  }

  @override
  bool shouldRepaint(DrawingPainter oldDelegate) => true;
}

class DrawingPoints {
  Paint paint;
  Offset points;
  DrawingPoints({this.points, this.paint});
}

enum SelectedMode { StrokeWidth, Opacity, Color }
