import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './scroller_text.dart';

class ScrollerTextWidget extends StatefulWidget {
  @override
  _ScrollerTextWidgetState createState() => _ScrollerTextWidgetState();
}

class _ScrollerTextWidgetState extends State<ScrollerTextWidget> {
  @override
  Widget build(BuildContext context) {
    final _scrollText = Provider.of<ScrollerText>(context);
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.all(16.0),
      child: Text(
        _scrollText.getText(),
        style: TextStyle(
          fontSize: 24.0,
          fontWeight: FontWeight.w700
        ),
      ),
    );
  }
}