import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../screens/dashboard/admin_dashboard.dart';

class BottomSheetWidget extends StatefulWidget {
  final Function update;
  final Function toggleScrollPlay;
  final Function setScrollDir;
  final Function incrementFontSize;
  final Function decrementFontSize;
  final Function setFontColor;
  final Function setBackgroundColor;
  final Function sendData;
  final Function toggleTextBlink;
  final Function setTextBlinkFrequency;
  final Function toggleBackgroundBlink;
  final Function setBackgroundBlinkFrequency;

  final Function toggleFlash;

  BottomSheetWidget(
    this.update, 
    this.toggleScrollPlay,
    this.setScrollDir,
    this.incrementFontSize, 
    this.decrementFontSize, 
    this.setFontColor, 
    this.toggleTextBlink, 
    this.setTextBlinkFrequency, 
    this.setBackgroundColor, 
    this.toggleBackgroundBlink, 
    this.setBackgroundBlinkFrequency, 
    this.toggleFlash,
    this.sendData
    );

  @override
  _BottomSheetWidgetState createState() => _BottomSheetWidgetState();
}
class _BottomSheetWidgetState extends State<BottomSheetWidget> {
  double _fontBlinkFrequency;
  double _backgroundBlinkFrequency;
  double _flashBlinkFrequency;
  double _flashIntensity;
  bool _fontBlinkSwitch;
  bool _backgroundBlinkSwitch;
  bool _flashOn;
  bool _scrollAnimationPlay;
  AdminDashboardState parent;
  

 TextEditingController scrollingTextController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _scrollAnimationPlay = false;
    _fontBlinkSwitch = false;
    _backgroundBlinkSwitch = false;
    _fontBlinkFrequency = 0;
    _backgroundBlinkFrequency = 0;
    _flashBlinkFrequency = 0;
    _flashIntensity = 0;
    _flashOn = false;
  }

  @override
  Widget build(BuildContext context) {
    return SlidingUpPanel(
            panel: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(24.0),
                  topRight: Radius.circular(24.0)
                )
              ),
              margin: const EdgeInsets.only( left: 15, right: 15),
              height: 500,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsetsDirectional.only(top:25.0),
                  ),
                  Text(
                    'Control Panel',
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w600,
                      color: Colors.blueGrey,
                      fontSize: 26
                    ),
                  ),
                  Padding(
                    padding: EdgeInsetsDirectional.only(top:15.0),
                  ),
                  TextField(
                    obscureText: false,
                    controller: scrollingTextController,
                    onChanged: (text) {
                      print("Text is: $text");
                      this.widget.update(text);
                    },
                    style: TextStyle(
                      fontSize: 18,
                      fontFamily: 'Poppins'
                    ),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(10, 15, 10, 15),
                      hintText: 'Enter Target Text',
                      hintStyle: TextStyle(color: Color.fromRGBO(0, 0, 0, 80), fontWeight: FontWeight.w100)
                    ),
                  ),
                  Expanded(
                    child: ListView(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      children: <Widget>[
                        ExpansionTile(
                          title: Row(
                            children: <Widget>[
                              Icon(Icons.compare_arrows),
                              Padding(padding: EdgeInsets.only(left: 10.0),),
                              Text(
                                'Scroll Direction Settings',
                                style: TextStyle(
                                  fontSize: 19.0,
                                  fontFamily: 'Poppins',
                                ),
                                )
                            ], 
                          ),
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.fromLTRB(0, 2, 0, 2),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    child: Center(
                                      child: Text('Scroll Direction'),
                                    ),
                                  ),
                                  Column(
                                    children: <Widget>[
                                      RawMaterialButton(
                                        elevation: 2.0,
                                          fillColor: Colors.black,
                                          shape: CircleBorder(),
                                          onPressed: (){
                                            this.widget.setScrollDir('up');
                                          },
                                          child: Icon(Icons.keyboard_arrow_up, color: Colors.white,),
                                      ),
                                      Wrap(
                                        direction: Axis.horizontal,
                                        spacing: 0.0, // gap between adjacent buttons
                                        runSpacing: 0.0,  
                                        children: <Widget>[
                                          RawMaterialButton(
                                        elevation: 2.0,
                                          fillColor: Colors.black,
                                          shape: CircleBorder(),
                                          onPressed: (){
                                            this.widget.setScrollDir('left');
                                          },
                                          child: Icon(Icons.keyboard_arrow_left, color: Colors.white,),
                                      ),
                                      RawMaterialButton(
                                        elevation: 2.0,
                                          fillColor: Colors.black,
                                          shape: CircleBorder(),
                                          onPressed: (){
                                            setState(() {
                                              _scrollAnimationPlay = !_scrollAnimationPlay;
                                            });
                                            this.widget.toggleScrollPlay();

                                            print('D Pad $_scrollAnimationPlay');
                                          },
                                          child: _scrollAnimationPlay?  Icon(Icons.pause, color: Colors.white,): Icon(Icons.play_arrow, color: Colors.white,),
                                      ),
                                      RawMaterialButton(
                                        elevation: 2.0,
                                          fillColor: Colors.black,
                                          shape: CircleBorder(),
                                          onPressed: (){
                                            this.widget.setScrollDir('right');
                                          },
                                          child: Icon(Icons.keyboard_arrow_right, color: Colors.white,),
                                      ),
                                      
                                        ],
                                      ),
                                      RawMaterialButton(
                                        elevation: 2.0,
                                          fillColor: Colors.black,
                                          shape: CircleBorder(),
                                          onPressed: (){
                                            this.widget.setScrollDir('down');
                                          },
                                          child: Icon(Icons.keyboard_arrow_down, color: Colors.white,),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        ExpansionTile(
                          title: Row(
                            children: <Widget>[
                              Icon(Icons.font_download),
                              Padding(padding: EdgeInsets.only(left: 10.0),),
                              Text(
                                'Font Settings',
                                style: TextStyle(
                                  fontSize: 19.0,
                                  fontFamily: 'Poppins',
                                ),
                                )
                            ], 
                          ),
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 2, 20, 2),
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.format_size),
                                  Text(
                                    'Font-Style',
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 16.0
                                    ),
                                  ),
                                  ButtonBar(
                                    alignment: MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      RawMaterialButton(
                                        shape: CircleBorder(),
                                        elevation: 2.0,
                                        fillColor: Colors.black,
                                        child: Center(
                                          child: Text(
                                            '+',
                                            style: TextStyle(
                                              fontSize: 22.0,
                                              color: Colors.white
                                            ),
                                          ),
                                        ),
                                        onPressed: () {
                                          print('Increase Font Size');
                                          this.widget.incrementFontSize();
                                        },
                                      ),
                                      RawMaterialButton(
                                        shape: CircleBorder(),
                                        elevation: 2.0,
                                        fillColor: Colors.black,
                                        child: Center(
                                          child: Text(
                                            '-',
                                            style: TextStyle(
                                              fontSize: 22.0,
                                              color: Colors.white
                                            ),
                                          ),
                                        ),
                                        onPressed: () {
                                          print('Decrease Font Size');
                                          this.widget.decrementFontSize();
                                        },
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.format_color_text),
                                  Text(
                                    'Font Color',
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 16.0
                                    ),
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      RawMaterialButton(
                                        elevation: 2.0,
                                        fillColor: Colors.black,
                                        shape: CircleBorder(),
                                        onPressed: (){
                                          print('Change Font Color');
                                          showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return AlertDialog(
                                                titlePadding: const EdgeInsets.all(0.0),
                                                contentPadding: const EdgeInsets.all(0.0),
                                                content: SingleChildScrollView(
                                                  child: ColorPicker(
                                                    pickerColor: Color(0xFFC5CAE9),
                                                    onColorChanged: this.widget.setFontColor,
                                                    colorPickerWidth: 300.0,
                                                    pickerAreaHeightPercent: 0.7,
                                                    enableAlpha: true,
                                                    displayThumbColor: true,
                                                    enableLabel: true,
                                                    paletteType: PaletteType.hsv,
                                                  ),
                                                ),
                                              );
                                            },
                                          );
                                        },
                                        child: Icon(Icons.color_lens, color: Colors.white,),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.lightbulb_outline, color: Colors.yellow,),
                                  Text(
                                    'Blink',
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 16.0
                                    ),
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Switch(
                                        value: _fontBlinkSwitch,
                                        onChanged: (bool fontBlink) {
                                          print("Font Blink $fontBlink");
                                          setState(() {
                                            _fontBlinkSwitch = fontBlink;
                                          });
                                          this.widget.toggleTextBlink(fontBlink);
                                        },
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.watch_later, color: Colors.black,),
                                  Text(
                                    'Blink Frequency',
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 16.0
                                    ),
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Slider(
                                        min: -0.1,
                                        max: 100.0,
                                        divisions: 100,
                                        value: _fontBlinkFrequency != null? _fontBlinkFrequency: 0.0,
                                        onChanged: (double fontBlinkFreq) {
                                          print("Font Blink ${fontBlinkFreq.round()}");
                                          setState(() {
                                            _fontBlinkFrequency = fontBlinkFreq.roundToDouble();
                                          });
                                          this.widget.setTextBlinkFrequency(fontBlinkFreq.round());
                                        },
                                      )
                                    ],
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        ExpansionTile(
                          title: Row(
                            children: <Widget>[
                              // Icon(Icons.crop_square),
                              Padding(padding: EdgeInsets.only(left: 10.0),),
                              Text(
                                'Background Settings',
                                style: TextStyle(
                                  fontSize: 19.0,
                                  fontFamily: 'Poppins',
                                ),
                                )
                            ], 
                          ),
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.format_color_text),
                                  Text(
                                    'Background Color',
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 16.0
                                    ),
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      RawMaterialButton(
                                        elevation: 2.0,
                                        fillColor: Colors.black,
                                        shape: CircleBorder(),
                                        onPressed: (){
                                          print('Change Bck Color');
                                          showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return AlertDialog(
                                                titlePadding: const EdgeInsets.all(0.0),
                                                contentPadding: const EdgeInsets.all(0.0),
                                                content: SingleChildScrollView(
                                                  child: ColorPicker(
                                                    pickerColor: Color(0xFFC5CAE9),
                                                    onColorChanged: this.widget.setBackgroundColor,
                                                    colorPickerWidth: 300.0,
                                                    pickerAreaHeightPercent: 0.7,
                                                    enableAlpha: true,
                                                    displayThumbColor: true,
                                                    enableLabel: true,
                                                    paletteType: PaletteType.hsv,
                                                  ),
                                                ),
                                              );
                                            },
                                          );
                                        },
                                        child: Icon(Icons.color_lens, color: Colors.white,),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.lightbulb_outline, color: Colors.yellow,),
                                  Text(
                                    'Blink Background',
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 16.0
                                    ),
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Switch(
                                        value: _backgroundBlinkSwitch,
                                        onChanged: (bool backgroundBlink) {
                                          setState(() {
                                            _backgroundBlinkSwitch = backgroundBlink;
                                          });
                                          this.widget.toggleBackgroundBlink(backgroundBlink);
                                        },
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.watch_later, color: Colors.black,),
                                  Text(
                                    'Blink Frequency',
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 16.0
                                    ),
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Slider(
                                        min: -0.1,
                                        max: 100.0,
                                        divisions: 100,
                                        value: _backgroundBlinkFrequency != null? _backgroundBlinkFrequency: 0.0,
                                        onChanged: (double backgroundBlinkFreq) {
                                          print("Background Blink ${backgroundBlinkFreq.round()}");
                                          setState(() {
                                            _backgroundBlinkFrequency = backgroundBlinkFreq.roundToDouble();
                                          });
                                          this.widget.setBackgroundBlinkFrequency(backgroundBlinkFreq.toInt());
                                        },
                                      )
                                    ],
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        ExpansionTile(
                          title: Row(
                            children: <Widget>[
                              Image.asset('images/fireworks.png', scale: 2.3,),
                              Padding(padding: EdgeInsets.symmetric(horizontal: 10.0),),
                              Text(
                                'Event Settings',
                                style: TextStyle(
                                  fontSize: 19.0,
                                  fontFamily: 'Poppins',
                                ),
                              )
                            ], 
                          ),
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.face, color: Colors.black54,),
                                  Padding(padding: EdgeInsets.symmetric(horizontal: 10)),
                                  Text(
                                    'Feeling',
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 16.0
                                    ),
                                  ),
                                  Padding(padding: EdgeInsets.symmetric(horizontal: 20)),
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      RawMaterialButton(
                                        elevation: 2.0,
                                        fillColor: Colors.white,
                                        shape: CircleBorder(),
                                        onPressed: (){
                                          print('Happy Face feeling Button');
                                        },
                                        child: Icon(Icons.tag_faces, color: Colors.yellow,),
                                      ),
                                      RawMaterialButton(
                                        elevation: 2.0,
                                        fillColor: Colors.white,
                                        shape: CircleBorder(),
                                        onPressed: (){
                                          print('Heart feeling button');
                                        },
                                        child: Icon(Icons.favorite, color: Colors.pink,),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.mic, color: Colors.black,),
                                  Padding(padding: EdgeInsets.symmetric(horizontal: 7)),
                                  Text(
                                    'Record Voice',
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 16.0
                                    ),
                                  ),
                                  Padding(padding: EdgeInsets.symmetric(horizontal: 7),),
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      RawMaterialButton(
                                        elevation: 2.0,
                                        splashColor: const Color(0x99FF0000),
                                        padding: EdgeInsets.all(10),
                                        onPressed: () {
                                          print('Recording Sent');
                                          final snackBar = SnackBar(
                                            content: Text('Recording Audio'),
                                            action: SnackBarAction(
                                              label: 'Close',
                                              onPressed: () {
                                                // Some code to undo the change.
                                              },
                                            ),
                                          );
                                          Scaffold.of(context).showSnackBar(snackBar);
                                        },
                                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20), side: BorderSide(color: Colors.black,)),
                                        child: GestureDetector(
                                          onLongPress: (){
                                            print('Recording Now...');
                                              var snackBar = SnackBar(
                                                content: Text('Recording Now'),
                                                action: SnackBarAction(
                                                  label: 'Close',
                                                  onPressed: () {
                                                    // Some code to undo the change.
                                                  },
                                                ),
                                              );
                                              Scaffold.of(context).showSnackBar(snackBar);
                                            },
                                          onLongPressEnd: (LongPressEndDetails details) {
                                            print('Recording Saved.');
                                            var snackBar = SnackBar(
                                              content: Text('Recording Saved'),
                                              action: SnackBarAction(
                                                label: 'Close',
                                                onPressed: () {
                                                  // Some code to undo the change.
                                                },
                                              ),
                                            );
                                            Scaffold.of(context).showSnackBar(snackBar);
                                          },
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Icon(Icons.fiber_manual_record, color: Color(0xFFEA0000),),
                                              Padding(padding: EdgeInsets.symmetric(horizontal: 5),),
                                              Text(
                                                'Hold to Record'
                                              )
                                            ],
                                          ),
                                        )
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                              child: Row(
                                children: <Widget>[
                                  Image.asset('images/slot-machine.png', scale: 8,),
                                  Padding(padding: EdgeInsets.symmetric(horizontal: 7)),
                                  Text(
                                    'Lucky Draw',
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 16.0
                                    ),
                                  ),
                                  Row(
                                    children: <Widget>[
                                      RawMaterialButton(
                                        padding: EdgeInsets.all(10),
                                        elevation: 2.0,
                                        fillColor: Colors.white,
                                        shape: CircleBorder(),
                                        onPressed: () {
                                          print('ABC Button Lucky Draw');
                                        },
                                        child: Text('ABC'),
                                      ),
                                      RawMaterialButton(
                                        padding: EdgeInsets.all(10),
                                        elevation: 2.0,
                                        fillColor: Colors.white,
                                        shape: CircleBorder(),
                                        onPressed: () {
                                          print('123 Button Lucky Draw');
                                        },
                                        child: Text('123'),
                                      ),
                                      //TO-DO: Add third button
                                    ],
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        ExpansionTile(
                          title: Row(
                            children: <Widget>[
                              Icon(Icons.flash_on, color: Colors.orange,),
                              Padding(padding: EdgeInsets.only(left: 10.0),),
                              Text(
                                'Flashlight Settings',
                                style: TextStyle(
                                  fontSize: 19.0,
                                  fontFamily: 'Poppins',
                                ),
                                )
                            ], 
                          ),
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.format_color_text),
                                  Text(
                                    'Light Intensity',
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 16.0
                                    ),
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Slider(
                                        min: -0.1,
                                        max: 100.0,
                                        divisions: 100,
                                        value: _flashIntensity != null? _flashIntensity: 0.0,
                                        onChanged: (double flashIntensity) {
                                          print("Flash Intensity ${flashIntensity.round()}");
                                          setState(() {
                                            _flashIntensity = flashIntensity.roundToDouble();
                                          });
                                        },
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.lightbulb_outline, color: Colors.yellow,),
                                  Text(
                                    'Blink',
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 16.0
                                    ),
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Switch(
                                        value: _flashOn,
                                        onChanged: (bool flashBlink) {
                                          setState(() {
                                            _flashOn = flashBlink;
                                          });
                                          this.widget.toggleFlash(flashBlink);
                                          print("Flash Blink $flashBlink");
                                        },
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.watch_later, color: Colors.black,),
                                  Text(
                                    'Blink Frequency',
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 16.0
                                    ),
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      Slider(
                                        min: -0.1,
                                        max: 100.0,
                                        divisions: 100,
                                        value: _flashBlinkFrequency != null? _flashBlinkFrequency: 0.0,
                                        onChanged: (double flashBlinkFreq) {
                                          print("Flash Blink ${flashBlinkFreq.round()}");
                                          setState(() {
                                            _flashBlinkFrequency = flashBlinkFreq.roundToDouble();
                                          });
                                        },
                                      )
                                    ],
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 10),
                        ),
                        RawMaterialButton(
                          onPressed: (){
                            print('Send settings');
                            this.widget.sendData();
                          },
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                          fillColor: Colors.redAccent,
                          splashColor: Colors.white60,
                          child: Container(
                            height: 40,
                            child: Center(
                              child: Text(
                                'SEND',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontFamily: 'Poppins'
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 10),
                        ),
                      ],
                    ),
                  ),
                ]
              ),
            ),
            collapsed: Container(
              decoration: BoxDecoration(
                color: Colors.blueGrey,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(24.0),
                  topRight: Radius.circular(24.0),
                )
              ),
              child: Center(
                child: Text(
                  "Pull Up to access Adminstrator Controls",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(24.0),
                  topRight: Radius.circular(24.0),
                ),
          );
    }
}