import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'websockets.dart';

///
/// Again, application-level global variable
///
AppCommunication app = new AppCommunication();

class AppCommunication {
  static final AppCommunication _app = new AppCommunication._internal();

  ///
  /// At first initialization, the user has not yet provided any name
  ///
  String _userName = "";

  ///
  /// Before the "join" action, the user has no unique ID
  ///
  String _userID = "";

  factory AppCommunication(){
    return _app;
  }

  AppCommunication._internal(){
    ///
    /// Let's initialize the WebSockets communication
    ///
    sockets.initCommunication();

    ///
    /// and ask to be notified as soon as a message comes in
    /// 
    sockets.addListener(_onMessageReceived);
  }

  ///
  /// Getter to return the user's name
  ///
  String get userName => _userName;

  /// ----------------------------------------------------------
  /// Common handler for all received messages, from the server
  /// ----------------------------------------------------------
  _onMessageReceived(serverMessage){
    ///
    /// As messages are sent as a String
    /// let's deserialize it to get the corresponding
    /// JSON object
    ///
    Map message = json.decode(serverMessage);

    switch(message["action"]){
      ///
      /// When the communication is established, the server
      /// returns the unique identifier of the user.
      /// Let's record it
      ///
      case 'connect':
        _userID = message["data"];
        break;

      ///
      /// For any other incoming message, we need to
      /// dispatch it to all the listeners
      ///
      default:
        _listeners.forEach((Function callback){
          callback(message);
        });
        break;
    }
  }

  /// ----------------------------------------------------------
  /// Common method to send requests to the server
  /// ----------------------------------------------------------
  send(String action, String data){
    print("In Send");
    ///
    /// When a user joins, we need to record the name
    /// he provides
    ///
    if (action == 'join'){
      _userName = data;
    }

    ///
    /// Send the action to the server
    /// To send the message, we need to serialize the JSON 
    ///
    sockets.send(json.encode({
      "action": action,
      "data": data
    }));
  }

  /// ==========================================================
  ///
  /// Listeners to allow the different pages to be notified
  /// when messages come in
  ///
  ObserverList<Function> _listeners = new ObserverList<Function>();

  /// ---------------------------------------------------------
  /// Adds a callback to be invoked in case of incoming
  /// notification
  /// ---------------------------------------------------------
  addListener(Function callback){
    _listeners.add(callback);
  }
  removeListener(Function callback){
    _listeners.remove(callback);
  }
}