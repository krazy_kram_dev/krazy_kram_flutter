import 'package:flutter/material.dart';

class ScrollerText with ChangeNotifier {
  String _text;

  ScrollerText(this._text);

  getText() => _text;
  setText(String text) => _text = text;

  void updateText(String text) {
    _text = text;
    notifyListeners();
  }
}